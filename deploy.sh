#!/bin/sh
# SPDX-License-Identifier: BSD-3-Clause
# SPDX-FileCopyrightText: 2016-2022 Harald Sitter <sitter@kde.org>

set -ex

bin='projects-api'
service='projects-kde-org-api'

user='projects'
ssh_target="$user@phoeni.kde.org"
ssh="ssh $ssh_target --"

home="/home/$user"
path="${home}/api/"
binpath="${home}/api/bin/"
systemd="${home}/.config/systemd/user"

$ssh mkdir -p $path
$ssh mkdir -p $binpath
$ssh mkdir -p $systemd

rsync -avz --progress -e ssh $bin $ssh_target:$binpath

rsync -avz --progress -e ssh systemd/$service.service $ssh_target:$systemd
rsync -avz --progress -e ssh systemd/$service.socket $ssh_target:$systemd

$ssh systemctl --user daemon-reload
$ssh systemctl --user enable $service.socket
$ssh systemctl --user stop $service.service || true
$ssh systemctl --user restart $service.socket

rsync -avz --progress -e ssh doc $ssh_target:$path/
