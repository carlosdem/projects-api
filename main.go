// SPDX-FileCopyrightText: 2017-2022 Harald Sitter <sitter@kde.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"runtime/debug"
	"syscall"
	"time"

	"invent.kde.org/sysadmin/projects-api.git/apis"
	"invent.kde.org/sysadmin/projects-api.git/daos"
	"invent.kde.org/sysadmin/projects-api.git/services"

	"github.com/coreos/go-systemd/activation"
	"github.com/getsentry/sentry-go"
	sentrygin "github.com/getsentry/sentry-go/gin"
	"github.com/gin-gonic/gin"
)

func SentryTransactor() gin.HandlerFunc {
	return func(c *gin.Context) {
		span := sentry.StartSpan(c, c.Request.RequestURI, sentry.TransactionName(c.Request.RequestURI))
		c.Next()
		span.Finish()
	}
}

func Version() string {
	if info, ok := debug.ReadBuildInfo(); ok {
		for _, setting := range info.Settings {
			if setting.Key == "vcs.revision" {
				return setting.Value
			}
		}
	}
	return ""
}

func main() {
	flag.Parse()

	fmt.Printf("Ready to rumble [%s]...\n", Version())

	if err := sentry.Init(sentry.ClientOptions{
		Dsn:              "https://bb761f0ff6ae4c5c8ff07f5c761e49c9@errors-eval.kde.org/26",
		TracesSampleRate: 0.25,
		AttachStacktrace: true,
		Release:          Version(),
	}); err != nil {
		fmt.Printf("Sentry initialization failed: %v\n", err)
	}
	defer sentry.Flush(2 * time.Second)

	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	router.Use(sentrygin.New(sentrygin.Options{Repanic: true}))
	router.Use(SentryTransactor())

	router.GET("/", func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "http://"+c.Request.Host+"/doc")
	})
	router.StaticFS("/doc", http.Dir("doc"))

	router.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "OK")
	})
	router.GET("/version", func(c *gin.Context) {
		c.String(http.StatusOK, Version())
	})

	v1 := router.Group("/v1")
	{
		gitDAO := daos.NewGitDAO()
		apis.ServeGitResource(v1, services.NewGitService(gitDAO))
		apis.ServeProjectResource(v1, services.NewProjectService(gitDAO))
	}

	listeners, err := activation.Listeners()
	if err != nil {
		panic(err)
	}

	log.Println("starting servers")
	var servers []*http.Server
	for _, listener := range listeners {
		server := &http.Server{Handler: router}
		go server.Serve(listener)
		servers = append(servers, server)
	}

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	// Wait for some quit cause.
	// This could be INT, TERM, QUIT or the db update trigger.
	// We'll then do a zero downtime shutdown.
	// This relies on systemd managing the socket and us doing graceful listener
	// shutdown. Once we are no longer listening, the system starts backlogging
	// the socket until we get restarted and listen again.
	// Ideally this results in zero dropped connections.
	<-quit
	log.Println("servers are shutting down")

	for _, srv := range servers {
		ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
		defer cancel()
		srv.SetKeepAlivesEnabled(false)
		if err := srv.Shutdown(ctx); err != nil {
			log.Fatalf("Server Shutdown: %s", err)
		}
	}

	log.Println("Server exiting")
}
