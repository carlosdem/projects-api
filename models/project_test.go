// SPDX-FileCopyrightText: 2020 Sandro Knauß <knauss@kde.org>
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInferEmptyBugzilla(t *testing.T) {
	b := BugzillaData{}
	p := Project{
	}
	b.Infer(&p)
	assert.Equal(t, "", p.BugDatabase)
	assert.Equal(t, "", p.BugSubmit)
}

func TestInferBugzilla(t *testing.T) {
	b := BugzillaData{
		Product: "foo",
		Component: "bar",
	}
	p := Project{
	}
	b.Infer(&p)
	assert.Equal(t, "https://bugs.kde.org/buglist.cgi?component=bar&product=foo&resolution=---", p.BugDatabase)
	assert.Equal(t, "https://bugs.kde.org/enter_bug.cgi?component=bar&product=foo", p.BugSubmit)
}
