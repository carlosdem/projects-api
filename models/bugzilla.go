// SPDX-FileCopyrightText: 2020 Sandro Knauß <knauss@kde.org>
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

package models

// BugzillaData represents the Bugzilla relevant data.
type BugzillaData struct {
	Product string `yaml:"product" json:"product"`
	Component string `yaml:"component" json:"component,omitempty"`
}
